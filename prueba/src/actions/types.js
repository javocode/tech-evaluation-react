export const SEND_DATA = 'SEND_DATA';
export const CHANGE_QUERY = 'CHANGE_QUERY';
export const GET_FIELDS = 'GET_FIELDS';
export const SHOW_INPUTS = 'SHOW_INPUTS';
export const CHANGE_FIELDS = 'CHANGE_FIELDS';
export const SET_FIELDS = 'SET_FIELDS';
import { SEND_DATA, CHANGE_QUERY, GET_FIELDS, SHOW_INPUTS, CHANGE_FIELDS, SET_FIELDS } from './types';
import axios from 'axios';

const URI = "http://localhost:8080/";

export const getQuery = () => (dispatch) => {
    dispatch({
        type: GET_FIELDS,
        payload: { query: "" }
    });
}

export const onChangeQueryContent = (params) => (dispatch) => {
    dispatch({
        type: CHANGE_QUERY,
        payload: params
    })
};

export const onChangeFields = (params) => (dispatch) => {
    dispatch({
        type: CHANGE_FIELDS,
        payload: params
    })
};

export const showInputs = (params) => (dispatch) => {
    dispatch({
        type: SHOW_INPUTS,
        payload: params
    })
    dispatch({
        type: SET_FIELDS,
        payload: params
    })
};

export const sendFields = (params) => async (dispatch) => {
    const response = await axios.post(`${URI}query`, params);
    console.log("Send:", response);
    if (response) {
        dispatch({
            type: SEND_DATA,
            payload: response.data
        })
    }
}
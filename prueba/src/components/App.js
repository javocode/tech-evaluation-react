import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from '../store';
import InputForm from './InputForm';

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="container">
                    <h1>Technical Evaluation | UTEC</h1>
                    <InputForm />
                </div>
            </Provider>
        )
    }
};

export default App;
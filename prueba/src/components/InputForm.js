import React, { Component } from 'react';
import { getQuery, onChangeQueryContent, sendFields, showInputs } from '../actions/queryAction';
import { connect } from 'react-redux';
import SelfGeneratedForm from './SelfGeneratedForm';

class InputForm extends Component {

    componentWillMount() {
        this.props.getQuery();
    }

    handleInput = (e) => {
        e.preventDefault();
        const { formData } = this.props;
        const formUpdated = { ...formData, [e.target.name]: e.target.value };
        this.props.onChangeQueryContent(formUpdated);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.parseQuery(this.props.formData.query);
    }

    parseQuery = (query) => {
        let MATCH_SLASH_AND_SPACE_PATTERN = /\(|\)| /g;
        let valuesText = query.substring(query.indexOf("(@"));
        let valuesWithCommas = valuesText.replace(MATCH_SLASH_AND_SPACE_PATTERN, "")
        let values = valuesWithCommas.split(",");
        
        let parameters = [];
        values.map((value) => {
            parameters.push({ [value.substring(1)]: value });
        })
        this.props.showInputs(parameters);
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor="queryContent">Insertar query (Insert o update)</label>
                    <textarea name="query" onChange={this.handleInput} className="form-control" id="queryContent" rows="3"></textarea>
                </div>
                <button onClick={this.handleSubmit} type="submit" className="btn btn-primary">Parser query</button>

                {
                    (this.props.fields.length > 0) ?
                        <SelfGeneratedForm fields={this.props.fields} />
                        :
                        null
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    formData: state.formDataReducer.queryData,
    fields: state.formDataReducer.fields
});

export default connect(mapStateToProps, { getQuery, onChangeQueryContent, sendFields, showInputs })(InputForm);
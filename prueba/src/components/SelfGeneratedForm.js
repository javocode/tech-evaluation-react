import React, { Component } from 'react';
import { onChangeFields, sendFields } from '../actions/queryAction';
import { connect } from 'react-redux';

class SelfGeneratedForm extends Component {

    handleInput = (e) => {
        e.preventDefault();
        const { dataToSend } = this.props;
        const field = {};
        const { name, value } = e.target;
        field[name] = value;
        const index = dataToSend.findIndex((item) => {
            return Object.keys(item)[0] === name;
        });
        dataToSend[index] = field;
        this.props.onChangeFields(dataToSend);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let parameters = [];
        for (var key in this.props.dataToSend) {
            var obj = this.props.dataToSend[key];
            parameters.push({
                "key": Object.keys(obj)[0],
                "description": Object.keys(obj)[0] + " del empleado"
            });
        }
        const data = {
            "query": this.props.query.query,
            "parameters": parameters
        }
        this.props.sendFields(data);
    }

    render() {
        return (
            <div>
                {this.props.fields.map((field, index) => {
                    const name = Object.keys(field)[0];
                    return (
                        <div key={index} className="form-group">
                            <label>{name}</label>
                            <input name={name} onChange={this.handleInput} type="text" className="form-control" aria-describedby="" />
                        </div>
                    )
                })}
                <button onClick={this.handleSubmit} type="submit" className="btn btn-primary">Send</button>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    dataToSend: state.formDataReducer.dataToSend,
    query: state.formDataReducer.queryData,
});

export default connect(mapStateToProps, { sendFields, onChangeFields })(SelfGeneratedForm);
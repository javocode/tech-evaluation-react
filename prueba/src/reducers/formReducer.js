import { SEND_DATA, CHANGE_QUERY, GET_FIELDS, SHOW_INPUTS, CHANGE_FIELDS, SET_FIELDS } from '../actions/types';

const initialState = {
    queryData: {},
    fields: [],
    dataToSend: [],
    dataReceived: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_FIELDS:
            return {
                ...state,
                formData: action.payload
            }
        case CHANGE_QUERY:
            return {
                ...state,
                queryData: action.payload
            }
        case SHOW_INPUTS:
            return {
                ...state,
                fields: action.payload
            }
        case CHANGE_FIELDS:
            return {
                ...state,
                dataToSend: action.payload
            }
        case SET_FIELDS:
            return {
                ...state,
                dataToSend: action.payload
            }
        case SEND_DATA:
            return {
                ...state,
                dataReceived: action.payload
            }
        default:
            return state
    }
}
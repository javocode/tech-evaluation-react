![https://www.utec.edu.pe/sites/default/files/logo_0.png](http://www.utec.edu.pe/sites/default/files/logo_utec_.png)

# Prueba Tecnica Frontend

El objetivo de esta evaluación es validar los skills de desarrollo que el area solicita.

# Pre requisitos

* Tener instalado npm y nodejs
* Tener instalado un cliente GIT
* Tener una cuenta en bitbucket

# Instrucciones

- Se solicitará desarrollar un solo formulario que cumpla con ciertas especificaciones.
- La prueba tiene una duración de 60 minutos.

# Especificaciones del formulario web

## Definicion

Se solicita desarrollar un formulario web usando react, angular o vue. Este formulario tiene 2 funcionalidades:

### Primera funcionalidad

- El usuario debe poder ingresar una consulta sql (insert)  en la caja de texto.
- Luego debe poder presionar el boton "Parse query"

![formulario-primera-parte.png](https://raw.githubusercontent.com/jrichardsz/static_resources/master/formulario-primera-parte.png)

### Segunda funcionalidad

- Al ser presionado el boton "Parse query", la query debe ser "analizada" para poder extraer cuantos parametros tiene.
- Los parametros tendran el patron : @name.
- La consulta string puede tener varios parametros : @name, @lastname, etc
- Una vez detectados los parametros, dee renderizar al usuario, una caja de texto por cada parametro para que se ingrese una descripcion:

![formulario-segunda-parte.png](https://raw.githubusercontent.com/jrichardsz/static_resources/master/formulario-segunda-parte.png)

# Criterios de evaluacion


* Uso de algun framework js como angular, react o vue (ultimas versiones)
* Correcto uso del package.json
* Calidad del codigo.
* Legibilidad del codigo.
* Upload del codigo al repositorio brindado usando git
* Sera un punto extra si el evaluador puede iniciar la app con solo ejecutar : npm run start
* Sera un punto extra si el evaluador puede iniciar la app en modo desarrollo con solo ejecutar : npm run dev
* Se considerará un plus si puede enviar la siguiente estructura json a un api usando ajax:

```json
{
 "query": "insert into ...",
 "parameters" : [
   {
     "key":"name",
     "description":"nombre del emepleado"
   },
   {
     "key":"lastname",
     "description":"apellido del empleado"
   },
   {
     "key":"job",
     "description":"trabajo del empleado"
   }
 ]
}
```
* Api para pruebas (https://github.com/jrichardsz/nodejs-express-snippets/blob/master/04-post-endpoint.js). Se validara que el json es recibido si es que esta api pinta en el log dicho json.
* Para correr el api, clonar el repositorio (https://github.com/jrichardsz/nodejs-express-snippets) y ejecutar npm install y node 04-post-endpoint.js
